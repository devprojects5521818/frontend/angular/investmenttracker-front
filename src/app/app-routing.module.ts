import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePortfolioComponent } from './components/create-portfolio/create-portfolio.component';
import { ExplorePortfolioComponent } from './components/explore-portfolio/explore-portfolio.component';
import { FnoTrackerComponent } from './components/fno-tracker/fno-tracker.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MutualfundTrackerComponent } from './components/mutualfund-tracker/mutualfund-tracker.component';
import { StockTrackerComponent } from './components/stock-tracker/stock-tracker.component';

const routes: Routes = [
  {path:'login',component: LoginComponent},
  {path:'portfolios/create', component: CreatePortfolioComponent},
  {path:'portfolios/explore', component: ExplorePortfolioComponent},
  {path:'home', component: HomeComponent},
  {path:'STOCKS/:id', component: StockTrackerComponent},
  {path:'MUTUAL FUNDS/:id', component: MutualfundTrackerComponent},
  {path:'FNO TRADE/:id', component: FnoTrackerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent,
  CreatePortfolioComponent,
  HomeComponent
];
