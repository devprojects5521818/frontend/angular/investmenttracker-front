import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../entities/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  loginRequest(data : User){
    return this.http.post<any>(environment.serverUrl+'/user/login/req',data);
  }

  verifyOtp(data : User){
    return this.http.post<any>(environment.serverUrl+'/user/login/otp/verify',data);
  }

}
