import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FNOReport } from '../entities/fnoreport';

@Injectable({
  providedIn: 'root'
})
export class FNOReportService {

  constructor(private http:HttpClient) { }
  
  addFNOReport(body: FNOReport){
    return this.http.post(environment.serverUrl+'/fno/addEntry',body);
  }

  getFNOReportByDate(body: string,portfolioId : string){
    return this.http.get<any>(environment.serverUrl+'/fno/getByTradedDate/' + body + '/' + portfolioId);
  }

  getFNOReportByDateRange(date1: string,date2: string,portfolioId :string){
    return this.http.get<any>(environment.serverUrl+'/fno/getByTradedDate/'+date1+'/'+date2+'/'+portfolioId);
  }

  getFNOReportByTradeResult(body: string,portfolioId : string){
    return this.http.get<any>(environment.serverUrl+'/fno/getByTradeResult/'+body+'/'+portfolioId);
  }

  getAllFNOReports(){
    return this.http.get<any>(environment.serverUrl+'/fno/getAll');
  }

  getAllFNOReportsByPortfolioIdAndType(id:string,type:string){
    return this.http.get<any>(environment.serverUrl+'/portfolio/getInvestment/'+id+'/'+type);
  }

  getFNOReportByDateAndResult(date1: string,result: string,portfolioId : string){
    return this.http.get<any>(environment.serverUrl+'/fno/getByDateAndResult/'+date1+'/'+result+'/'+portfolioId);
  }

  getFNOReportByDateRangeAndResult(date1: string,date2: string,portfolioId :string,result: string){
    return this.http.get<any>(environment.serverUrl+'/fno/getByDateRangeAndPortfolioIdAndResult/'+date1+'/'+date2+'/'+portfolioId+'/'+result);
  }

  deleteFnoReportsByIdAndType(id:string,type:string){
    return this.http.get<any>(environment.serverUrl+'/portfolio/deleteInvestment/'+id+'/'+type);
  }


}
