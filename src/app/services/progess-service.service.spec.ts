import { TestBed } from '@angular/core/testing';

import { ProgessServiceService } from './progess-service.service';

describe('ProgessServiceService', () => {
  let service: ProgessServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProgessServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
