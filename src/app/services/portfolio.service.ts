import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Portfolio } from '../entities/portfolio';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(private http: HttpClient) { }


  createPortfolio(data : Portfolio){
    return this.http.post<any>(environment.serverUrl+'/portfolio/create',data);
  }

  loadPortfoliosByEmail(email: string){
    return this.http.get<any>(environment.serverUrl+'/portfolio/getByEmail/' + email);
  }

  

}
