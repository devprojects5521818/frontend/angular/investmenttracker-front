import { TestBed } from '@angular/core/testing';

import { FNOReportService } from './fnoreport.service';

describe('FNOReportService', () => {
  let service: FNOReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FNOReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
