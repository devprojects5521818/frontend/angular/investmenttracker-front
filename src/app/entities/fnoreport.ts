export interface FNOReport {
    id?: string,
    tradedDate?: string,
    tradeResult?: string,
    grossAmount?: number,
    tradeCharges?: number,
    netAmount?: number,
    thirtyPercentShare?: number,
    seventyPercentShare?: number,
    finalInHandProfit?: number,
    portfolioId?: string
}
