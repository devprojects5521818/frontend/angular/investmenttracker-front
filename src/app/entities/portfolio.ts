export interface Portfolio {

    id? : string,
    portfolioName? : string,
    portfolioType? : string,
    createdBy? : string,
    portfolioOwners? : string[]

}
