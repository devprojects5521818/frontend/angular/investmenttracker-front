export interface User {
    id? : string,
    email? : string,
    otp? : string,
    requestType? : number,
}
