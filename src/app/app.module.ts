import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './module/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FnoTrackerComponent } from './components/fno-tracker/fno-tracker.component';
import { MutualfundTrackerComponent } from './components/mutualfund-tracker/mutualfund-tracker.component';
import { StockTrackerComponent } from './components/stock-tracker/stock-tracker.component';
import { ExplorePortfolioComponent } from './components/explore-portfolio/explore-portfolio.component';
import { CreatePortfolioComponent } from './components/create-portfolio/create-portfolio.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AddFnoEntryComponent } from './components/add-fno-entry/add-fno-entry.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    AddFnoEntryComponent,
    LoginComponent,
    HomeComponent,
    CreatePortfolioComponent,
    ExplorePortfolioComponent,
    StockTrackerComponent,
    MutualfundTrackerComponent,
    FnoTrackerComponent,
    ProgressBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
