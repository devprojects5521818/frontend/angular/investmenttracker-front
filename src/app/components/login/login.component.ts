import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { GenericResponse } from 'src/app/entities/generic-response';
import { User } from 'src/app/entities/user';
import { LoginService } from 'src/app/services/login.service';
import { ProgessServiceService } from 'src/app/services/progess-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private loginService:LoginService,
              private snackbar : MatSnackBar,
              private router : Router,
              private spinnerService: ProgessServiceService) { }

  isOtpGenerated = false;
  showSubmitButton = true;

  loginForm!: FormGroup;

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('',[Validators.required,Validators.email]),
      otp: new FormControl(),
    });

    console.warn(this.loginForm.controls.email.errors);
    
  }


  requestOtpForEmail(){
    let spin = this.spinnerService.open();
    let emailId = this.loginForm.controls.email.value;
    if(emailId != null || emailId != ""){
      let user : User = {
        email : emailId,
        requestType : 0,
      }
      this.loginService.loginRequest(user).subscribe((data:GenericResponse<any>) => {
        spin.close();
        if(data.code === 'OK' && data.body === true){
          this.isOtpGenerated = true,
          this.showSubmitButton = false;
          this.snackbar.open(data.message!,'close',{duration:2000});
        }
        this.snackbar.open(data.message!,'close',{duration:2000});
      },err => {
        spin.close();
        console.log(err);
        this.snackbar.open('Something Went Wrong','close',{duration:2000})
      });
    }else{
      this.snackbar.open('Please Enter a Valid Email Address','close',{duration:2000})
    }
  }


  verifyOtp(){
    let spin = this.spinnerService.open();
    let emailId = this.loginForm.controls.email.value;
    let otp = this.loginForm.controls.otp.value;
    if (otp != null && otp != "") {
      let user : User = {
        email : emailId,
        requestType : 1,
        otp: otp,
      }
      this.loginService.verifyOtp(user).subscribe((data:GenericResponse<any>) => {
        spin.close();
        if(data.code === 'OK' && data.body === true){
          localStorage.setItem('loggedInUser',emailId);
          this.snackbar.open(data.message!,'close',{duration:2000});
        }
        this.snackbar.open(data.message!,'close',{duration:2000});
        this.router.navigate(['home']);
      },err => {
        spin.close();
        console.log(err);
        this.snackbar.open('Something Went Wrong','close',{duration:2000})
      });
    } else {
      this.snackbar.open('Please Enter a Valid Otp','close',{duration:2000})
    }
  }


  get emailValidations(){
    return this.loginForm.get('email');
  }

  get otpValidations(){
    return this.loginForm.get('otp');
  }


}
