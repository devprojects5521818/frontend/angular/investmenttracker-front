import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FnoTrackerComponent } from './fno-tracker.component';

describe('FnoTrackerComponent', () => {
  let component: FnoTrackerComponent;
  let fixture: ComponentFixture<FnoTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FnoTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FnoTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
