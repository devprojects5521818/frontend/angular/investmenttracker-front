import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FNOReport } from 'src/app/entities/fnoreport';
import { GenericResponse } from 'src/app/entities/generic-response';
import { FNOReportService } from 'src/app/services/fnoreport.service';
import { AddFnoEntryComponent } from '../add-fno-entry/add-fno-entry.component';

@Component({
  selector: 'app-fno-tracker',
  templateUrl: './fno-tracker.component.html',
  styleUrls: ['./fno-tracker.component.scss']
})
export class FnoTrackerComponent implements OnInit {

  constructor(private fnoService: FNOReportService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private dateAdapter: DateAdapter<Date>,
    private route: ActivatedRoute,
    private router: Router) {
    this.dateAdapter.setLocale('en-GB');
  }

  fnoReportsList: FNOReport[] = [];
  displayedColumns: string[] = ['date', 'grossAmount', 'charges', 'netAmount', 'seventyPer', 'thirtyPer', 'finalInHandProfit', 'tradeResult', 'EDIT', 'DELETE'];
  defaultSelection = 'ALL';
  tradeResultList: string[] = [
    'ALL', 'PROFIT', 'LOSS', 'NO_TRADE',
  ];
  selectedTradeResult = "ALL";
  startDate = new FormControl();
  endDate = new FormControl();

  portfolioId: string = "";

  ngOnInit(): void {
    this.route.params.subscribe(e => {
      this.portfolioId = e.id;
    }, err => {
      console.log(err);
    });

    //this.getAllFnoReports();
    this.getAllFnoReportsByPortfolioIdAndType();

  }

  setTradeResult(data: any) {
    this.selectedTradeResult = data;
  }

  openDialog() {
    this.dialog.open(AddFnoEntryComponent, {
      data: this.portfolioId,
    });
  }

  /**
  * Gets FNO Reports From Start To End Without Any Filter
  */
  getAllFnoReports() {
    this.fnoService.getAllFNOReports().subscribe((data: FNOReport[]) => {
      this.fnoReportsList = data;
      this.snackBar.open('Data Loaded', 'close', { duration: 2000 });
    }, err => {
      this.snackBar.open('Error Occured ' + err, 'close', { duration: 2000 });
      console.log(err);
    });
  }


  /**
 * Gets FNO Reports From Start To End Without Any Filter
 */
  getAllFnoReportsByPortfolioIdAndType() {
    this.fnoService.getAllFNOReportsByPortfolioIdAndType(this.portfolioId, 'FNO TRADE').subscribe((data: GenericResponse<any>) => {
      if (data.code === 'OK') {
        this.fnoReportsList = data.body;
        this.snackBar.open(data.message!, 'close', { duration: 2000 });
      } else {
        this.snackBar.open(data.message!, 'close', { duration: 2000 });
      }
    }, err => {
      this.snackBar.open('Error Occured ' + err, 'close', { duration: 2000 });
      console.log(err);
    });
  }

  /**
   * Get Fno Reports For a Particular Date 
   */
  getAllFnoReportsByTradedDate(date: string) {
    this.fnoService.getFNOReportByDate(date,this.portfolioId).subscribe((data: FNOReport[]) => {
      this.fnoReportsList.splice(0);
      this.fnoReportsList = data;
      this.snackBar.open('Data Loaded', 'close', { duration: 2000 });
    }, err => {
      this.snackBar.open('Error Occured ' + err, 'close', { duration: 2000 });
      console.log(err);
    });
  }


  /**
   * Get Fno Reports For a Date Range
   */
  getAllFnoReportsByDateRange(startdate: string, endDate: string) {
    this.fnoService.getFNOReportByDateRange(startdate, endDate,this.portfolioId).subscribe((data: FNOReport[]) => {
      this.fnoReportsList = data;
      this.snackBar.open('Data Loaded', 'close', { duration: 2000 });
    }, err => {
      this.snackBar.open('Error Occured ' + err, 'close', { duration: 2000 });
      console.log(err);
    });
  }


  /**
   * Get Fno Reports By a Trade Result
   */
  getAllFnoReportsByTradeResult() {
    this.fnoService.getFNOReportByTradeResult(this.selectedTradeResult!,this.portfolioId).subscribe((data: FNOReport[]) => {
      this.fnoReportsList = data;
      this.snackBar.open('Data Loaded', 'close', { duration: 2000 });
    }, err => {
      this.snackBar.open('Error Occured ' + err, 'close', { duration: 2000 });
      console.log(err);
    });
  }


  getFNOReportByDateAndResult() {
    var datePipe = new DatePipe('en-GB');
    let startDate = null;
    if (this.startDate.value != null) {
      startDate = datePipe.transform(this.startDate.value, 'yyyy-MM-dd');
    }
    this.fnoService.getFNOReportByDateAndResult(startDate!, this.selectedTradeResult!,this.portfolioId).subscribe((data: FNOReport[]) => {
      this.fnoReportsList.splice(0);
      this.fnoReportsList = data;
    }, err => {
      console.log(err);
    });
  }

  getFNOReportByDateRangeAndResult() {
    var datePipe = new DatePipe('en-GB');
    let startDate = null;
    let endDate = null;

    if (this.startDate.value != null) {
      startDate = datePipe.transform(this.startDate.value, 'yyyy-MM-dd');
    }

    if (this.endDate.value != null) {
      endDate = datePipe.transform(this.endDate.value, 'yyyy-MM-dd');
    }

    this.fnoService.getFNOReportByDateRangeAndResult(startDate!, endDate!,this.portfolioId, this.selectedTradeResult!).subscribe((data: FNOReport[]) => {
      this.fnoReportsList.splice(0);
      this.fnoReportsList = data;
    }, err => {
      console.log(err);
    });
  }


  getDataAsPerFilter() {

    var datePipe = new DatePipe('en-GB');
    let startDate = null;
    let endDate = null;

    if (this.startDate.value != null) {
      startDate = datePipe.transform(this.startDate.value, 'yyyy-MM-dd');
    }

    if (this.endDate.value != null) {
      endDate = datePipe.transform(this.endDate.value, 'yyyy-MM-dd');
    }

    if(startDate === null && endDate != null ){
      alert('Please Select Start Date');
      return;
    }

    if (startDate != null && this.selectedTradeResult === 'ALL' && endDate === null) {
      this.getAllFnoReportsByTradedDate(startDate);
    } else if (startDate != null && this.selectedTradeResult != 'ALL' && endDate === null) {
      this.getFNOReportByDateAndResult();
    } else if (startDate != null && endDate != null && this.selectedTradeResult === 'ALL') {
      this.getAllFnoReportsByDateRange(startDate, endDate);
    } else if (startDate != null && endDate != null && this.selectedTradeResult != 'ALL') {
      this.getFNOReportByDateRangeAndResult();
    } else if (this.selectedTradeResult === 'ALL') {
      this.getAllFnoReportsByPortfolioIdAndType();
    } else if (this.selectedTradeResult != 'ALL') {
      this.getAllFnoReportsByTradeResult();
    }

  }

  editExistingFNOEntry(record: FNOReport) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(AddFnoEntryComponent, {
      data : record
    });
  }


  deleteById(recordId: string) {
    this.fnoService.deleteFnoReportsByIdAndType(recordId, 'FNO TRADE').subscribe((data: GenericResponse<any>) => {
      if (data.code === 'OK') {
        this.snackBar.open(data.message!, 'close', { duration: 2000 });

        this.ngOnInit();
        // this.router.navigate(['FNO TRADE/'+this.portfolioId])
      }
      this.snackBar.open(data.message!, 'close', { duration: 2000 });
    }, err => {
      console.log(err);

      this.snackBar.open('Something Went Wrong', 'close', { duration: 2000 })
    });
  }


}
