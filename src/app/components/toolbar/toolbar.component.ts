import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CreatePortfolioComponent } from '../create-portfolio/create-portfolio.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {

  }


  logout(){
    localStorage.clear();
    this.router.navigate(['login']);
  }

}
