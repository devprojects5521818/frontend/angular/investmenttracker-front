import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MutualfundTrackerComponent } from './mutualfund-tracker.component';

describe('MutualfundTrackerComponent', () => {
  let component: MutualfundTrackerComponent;
  let fixture: ComponentFixture<MutualfundTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MutualfundTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MutualfundTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
