import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mutualfund-tracker',
  templateUrl: './mutualfund-tracker.component.html',
  styleUrls: ['./mutualfund-tracker.component.scss']
})
export class MutualfundTrackerComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

  portfolioId: string = "";

  ngOnInit(): void {
    this.route.params.subscribe(e => {
      this.portfolioId = e.id;
      alert(this.portfolioId)
      console.log(this.portfolioId);
   },err =>{
      console.log(err);
   });

  }

}
