import { Component, OnInit } from '@angular/core';
import { GenericResponse } from 'src/app/entities/generic-response';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { Portfolio } from 'src/app/entities/portfolio';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-explore-portfolio',
  templateUrl: './explore-portfolio.component.html',
  styleUrls: ['./explore-portfolio.component.scss']
})
export class ExplorePortfolioComponent implements OnInit {

  constructor(private portfolioService : PortfolioService,
              private snackbar: MatSnackBar) { }

  userPortfolios : Portfolio[] = []

  ngOnInit(): void {
    this.getPortfoliosByUserEmail();
  }


  getPortfoliosByUserEmail(){
    let email = localStorage.getItem('loggedInUser');

    this.portfolioService.loadPortfoliosByEmail(email!).subscribe((data:GenericResponse<any>) => {
      if(data.code === 'OK' && data.body.length > 0){
        this.userPortfolios = data.body;

        this.snackbar.open(data.message!,'close',{duration:2000});
      }
      this.snackbar.open(data.message!,'close',{duration:2000});
    },err =>{
      console.log(err);
      this.snackbar.open('Something went wrong','close',{duration:2000});
    });
  }

}
