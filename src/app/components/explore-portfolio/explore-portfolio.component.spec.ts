import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExplorePortfolioComponent } from './explore-portfolio.component';

describe('ExplorePortfolioComponent', () => {
  let component: ExplorePortfolioComponent;
  let fixture: ComponentFixture<ExplorePortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExplorePortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExplorePortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
