import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { GenericResponse } from 'src/app/entities/generic-response';
import { Portfolio } from 'src/app/entities/portfolio';
import { PortfolioService } from 'src/app/services/portfolio.service';

@Component({
  selector: 'app-create-portfolio',
  templateUrl: './create-portfolio.component.html',
  styleUrls: ['./create-portfolio.component.scss']
})
export class CreatePortfolioComponent implements OnInit {

  createPortfolioForm!: FormGroup;

  constructor(
    private portfolioService : PortfolioService,
    private snackbar: MatSnackBar,
    private router: Router) { }

  PortfolioTypeList: string[] = [
    'STOCKS', 'MUTUAL FUNDS', 'FNO TRADE', 'LIC'
  ];

  selectedPortfolioType = "";

  setPortfolioType(data: any) {
    this.selectedPortfolioType = data;
  }

  ngOnInit(): void {
    this.createPortfolioForm = new FormGroup({
      portfolioName: new FormControl(null,[Validators.required]),
      portfolioType: new FormControl(null,[Validators.required]),
      createdBy: new FormControl(null,[Validators.required]),
    });

    let email = localStorage.getItem('loggedInUser');
    this.createPortfolioForm.controls.createdBy.setValue(email);
  }


  createPortfolio() {
    let portfolioName = this.createPortfolioForm.controls.portfolioName.value;
    let portfolioType = this.createPortfolioForm.controls.portfolioType.value;
    let createdBy = this.createPortfolioForm.controls.createdBy.value;

    let data : Portfolio = {
      portfolioName : portfolioName,
      portfolioType : portfolioType,
      createdBy : createdBy
    }

    this.portfolioService.createPortfolio(data).subscribe((data:GenericResponse<any>) => {
      if(data.code === 'OK'){
        this.snackbar.open(data.message!,'close',{duration:2000});
      }else{
        this.snackbar.open(data.message!,'close',{duration:2000});
      }
    },err=>{
      console.log(err);
      this.snackbar.open('Something Went Wrong','close',{duration:2000});
    });

    this.resetCreatePortfolioForm();

    this.router.navigate(['home']);

  }


  resetCreatePortfolioForm(){
    this.createPortfolioForm.reset();

    let email = localStorage.getItem('loggedInUser');
    this.createPortfolioForm.controls.createdBy.setValue(email);
  }

}
