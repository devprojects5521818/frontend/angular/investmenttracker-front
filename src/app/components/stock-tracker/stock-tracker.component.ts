import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-stock-tracker',
  templateUrl: './stock-tracker.component.html',
  styleUrls: ['./stock-tracker.component.scss']
})
export class StockTrackerComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

  portfolioId: string = "";

  ngOnInit(): void {

    this.route.params.subscribe(e => {
       this.portfolioId = e.id;
       alert(this.portfolioId)
       console.log(this.portfolioId);
    },err =>{
       console.log(err);
    });
 


  }

}
