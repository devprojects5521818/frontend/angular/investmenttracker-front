import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterOutlet } from '@angular/router';
import { FNOReport } from 'src/app/entities/fnoreport';
import { FNOReportService } from 'src/app/services/fnoreport.service';

@Component({
  selector: 'app-add-fno-entry',
  templateUrl: './add-fno-entry.component.html',
  styleUrls: ['./add-fno-entry.component.scss']
})
export class AddFnoEntryComponent implements OnInit {

  fnoEntryForm!: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddFnoEntryComponent>,
    private fnoService: FNOReportService,
    private snackbar: MatSnackBar,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: string,
    @Inject(MAT_DIALOG_DATA) public dataToBeEdited: FNOReport) { }

  ngOnInit(): void {


    this.fnoEntryForm = new FormGroup({
      tradeDate: new FormControl(),
      grossAmount: new FormControl(),
      charges: new FormControl(),
      tradeResult: new FormControl()
    });

    if (this.dataToBeEdited != null) {
 
      this.fnoEntryForm.patchValue({
        tradeDate: this.dataToBeEdited.tradedDate,
        grossAmount: this.dataToBeEdited.grossAmount,
        charges: this.dataToBeEdited.tradeCharges!,
        tradeResult: this.dataToBeEdited.tradeResult
      });
    }



  }

  tradeResultList: string[] = [
    'PROFIT', 'LOSS', 'NO_TRADE'
  ];

  selectedTradeResult = "";

  setTradeResult(data: any) {
    this.selectedTradeResult = data;
  }

  addFnoEntryInDb() {
    let tradedDate = this.fnoEntryForm.controls.tradeDate.value;
    let grossValue = this.fnoEntryForm.controls.grossAmount.value;
    let charges = this.fnoEntryForm.controls.charges.value;
    let result = this.fnoEntryForm.controls.tradeResult.value;

    var datePipe = new DatePipe('en-GB');
    let convertedDate = datePipe.transform(tradedDate, 'yyyy-MM-dd');

    let data: FNOReport = {
      tradedDate: convertedDate!,
      tradeResult: result,
      grossAmount: grossValue,
      tradeCharges: charges,
      portfolioId: this.data,
    }

    this.fnoService.addFNOReport(data).subscribe((data: FNOReport) => {
      if (data.id != null) {
        this.snackbar.open('Success', 'close', { duration: 2000 })
      }
    }, err => {
      console.log(err);
    });

    this.dialogRef.close();
    // this.router.navigate(['options']);
  }


  resetFnoRecordEntryForm() {
    this.fnoEntryForm.reset();
  }

}
