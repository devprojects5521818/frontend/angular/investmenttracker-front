import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFnoEntryComponent } from './add-fno-entry.component';

describe('AddFnoEntryComponent', () => {
  let component: AddFnoEntryComponent;
  let fixture: ComponentFixture<AddFnoEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFnoEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFnoEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
